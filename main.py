import random
from random import shuffle

class GenerarAleatorios:
    
    def __init__(self):
        self.n = 0 
        self.lista = []
        pass
    
    def lanzarDados(self):
        dado1 = random.randint(1,6)
        dado2 = random.randint(1,6)
        self.n = dado1 + dado2 
        return self.n 
    
    def mezclarCartas(self):
        lista = []
        for i in range(self.n):
            lista.append([])
            for j in range(5):
                number = random.randint(1,12)
                lista[i].append(number)
        self.lista = lista
        return self.lista    
        
        
class Ordenador(GenerarAleatorios):
    
    def ordenarLista(self):
        self.lista.sort()
        return self.lista
    
    def desordenarLista(self):
        shuffle(self.lista)
        return self.lista
    

class JugadorCartas(GenerarAleatorios):
    
    def repartirCartas(self, jugadores):
        listJugadores = []
        for i in range(jugadores):
            listJugadores.append(i+1)
        
        if len(self.lista) == len(listJugadores):
            for j in range(len(listJugadores)):
                print("Jugador ", j+1, ": ", self.lista[j])
            print("Sobraron 0 barajas y jugadores")
        elif len(self.lista) > len(listJugadores):
            for j in range(len(listJugadores)):
                print("Jugador ", j+1, ": ", self.lista[j])
            print("Sobraron ", len(self.lista) - len(listJugadores), " barajas")
        else:
            try:
                for j in range(len(listJugadores)):
                    print("Jugador ", j+1, ": ", self.lista[j])
            except:
                print("Sobraron ", len(listJugadores) - len(self.lista), " jugadores")
            

#Crea los objetos de las clases que quieras aquí              
                        
#Objeto de la clase JugadorCartas()
miObjeto = JugadorCartas()
print("Suma de dados: ", miObjeto.lanzarDados(), "\n")
print("Lista de barajas: ", miObjeto.mezclarCartas(), "\n")
print(miObjeto.repartirCartas(6)) #Debes ingresar un argumento